from bs4 import BeautifulSoup
import requests
import json
import pandas as pd
import re

cmc = requests.get('https://coinmarketcap.com')
soup = BeautifulSoup(cmc.content, 'html.parser')

table_MN = pd.read_html('https://coinmarketcap.com')
df = table_MN[0]
print(df[95:100])
def get_name(str_name):
    match = re.match(r"([a-z]+)([0-9])([a-z]+)", str_name.replace('Buy',''), re.I)
    if match:
        items = match.groups()
        return items[0]
    return


df['actual_name'] = df['Name'].apply(get_name)
# print(df['actual_name'])